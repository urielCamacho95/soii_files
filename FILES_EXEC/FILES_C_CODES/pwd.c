#include <stdio.h>
#include <stdlib.h>

#ifdef __unix__
    #define IS_POSIX 1
    #include <unistd.h>
#else
    #define IS_POSIX 0
#endif


int main (int argv, char **argc)
{
    if (IS_POSIX == 1) {
        char cwd[1024];
        getcwd(cwd, sizeof(cwd));
        puts("Ruta actual:");
        printf("\t %s\n", cwd);
    }
    return 0;
}