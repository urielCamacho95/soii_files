#! /bin/bash
	echo '$0 = ' $0
	echo '$1 = ' $1
	echo '$2 = ' $2
	if [ $# -gt 0 ]; then
    	if [ $# -gt 1 ]; then
    		nano
    	else
    		nano $2
    	fi
    		
	else
    	echo "Your command line contains no arguments"
	fi	
