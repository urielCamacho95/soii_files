#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>


int main(int argc, char * argv[]){
	if(argc==2){
		/*Deleting our file*/
		errno = 0;
		int fd;
		fd = unlink(argv[1]);
    	if(-1 == fd){
        	printf("\n Ocurrio un error al crear el archivo[%s]\n",strerror(errno));
        	return 1;
    	}else{
    	   	printf("\n El archivo %s se elimino correctamente\n",argv[1]);
        	/* open() succeeded, now one can do read operations
        	   on the file opened since we opened it in read-only
        	   mode. Also once done with processing, the file needs
    	       to be closed. Closing a file can be achieved using
        	   close() function. */
	    }

	}else{
		printf("\nDebes incluir el nombre del archivo");
	}
	return 0;
}