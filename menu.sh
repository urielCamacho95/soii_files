#! /bin/bash
opt="$1";
    shift;              #expose next argument
    case "$opt" in
        "crea_a" )
            echo 'Se creara el siguiente archivo' 
            echo $1;
            if [ $1 != '' ]
            then
                 ./FILES_EXEC/create_file $pwd$1
            else
                echo 'Debes incluir el nombre del nuevo archivo'
            fi  
         ;;
        "borra_a" )
           echo 'Se eliminara el siguiente archivo'
           echo $1;
            if [ $1 != '' ]
            then
                 ./FILES_EXEC/delete_file $pwd$1
            else
                echo 'Debes incluir el nombre del archivo'
            fi  
           ;;
        "mueve_a" )
            echo 'Se movera el siguiente archivo'
            echo $1;
            echo 'a la ruta '$2
            if [ $1 != '' ]
            then
                if [ $2 != '' ]
                then
                    if [ -f $1 ]
                    then
                    ./FILES_EXEC/move_file $pwd$1 $2
                    else
                        echo 'El archivo '$1' no existe'
                    fi
                else
                    echo 'Debes incluir el nombre del archivo y su destino'
                fi
            else
                echo 'Debes incluir el nombre del archivo y su destino'
            fi  

            ;;
        "renombra_a" )
            echo 'Se renombrara el siguiente archivo'
            echo $1;
            echo 'por: '$2
            if [ $1 != '' ]
            then
                if [ $2 != '' ]
                then
                   ./FILES_EXEC/rename $pwd$1 $pwd$2
                else
                    echo 'Debes incluir el nombre antiguo y el nuevo del archivo'
                fi
            else
                echo 'Debes incluir el nombre antiguo y el nuevo del archivo'
            fi    
            ;;
        "lpermisos_a" )
            echo 'Mostrando permisos'
            if [ $1 != '' ]
            then
                ./FILES_EXEC/permisos $pwd$1
            else
                echo 'Debes incluir el nombre del archivo'
            fi
            ;;
        "modif_a" )
            echo 'Se modificara el archivo'
            if [ $1 != '' ]
            then
                sh FILES_EXEC/edit.sh $pwd$1
            else
                nano
            fi
            
            ;;
        "muestra_a" )
            echo 'Se muestra el contenido del archivo'
            if [ $1 != '' ]
            then
                ./FILES_EXEC/cat $pwd$1
            else
                echo 'Debes incluir el nombre del archivo'
            fi    
            ;;

        "muestra_jerarq" )
            echo 'Se muestra la jerarquia del archivo'
            if [ $1 != '' ]
            then
                ./FILES_EXEC/tree $pwd
            else
                echo 'Debes incluir el nombre del archivo'
            fi 
            ;;

        "muestra_ruta" )
            echo 'Se muestra ruta actual del archivo '$1
            if [ $1 != '' ]
            then
                ./FILES_EXEC/pwd $1
            else
                echo 'Debes incluir el nombre del archivo'
            fi 
            ;;
        "crea_d" )
            echo 'Se creara el directorio '$1
            if [ $1 != '' ]
            then
                ./DIR_EXEC/create_dir $1
            else
                echo 'Debes incluir el nombre del directorio'
            fi     
            ;;
        "borra_d" )
            echo 'Se eliminara el directorio '$1
            if [ $1 != '' ]
            then
                ./DIR_EXEC/delete_dir $1
            else
                echo 'Debes incluir el nombre del directorio'
            fi
            ;;
        "mueve_d" )
            echo 'Se movera el directorio '$1
            echo ' a la ubicacion: '$2
            if [ $1 != '' ]
            then
                if [ $2 != '' ]
                then
                    mv $1 $2
                else
                    echo 'Debes incluir 2 parametros'
                fi
            else
                echo 'Debes incluir 2 parametros'
            fi
            ;;
        "renombra_d" )
            echo 'Se cambiara el nombre del directorio '$1
            echo 'por: '$2
            if [ $1 != '' ]
            then
                if [ $2 != '' ]
                then
                    mv $1 $2
                else
                    echo 'Debes incluir 2 parametros'
                fi
            else
                echo 'Debes incluir 2 parametros'
            fi
            ;;
        "lpermisos_d" )
            if [ $1 != '' ]
            then
                ls -la | grep $1
            else
                echo 'Debes incluir el nombre del directorio'
            fi    
            ;;
        *) echo >&2 "Opcion invalida: $opt"; exit 1;;
   esac