#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>


int main(int argc, char * argv[]){
	int val=0;
	if(argc==2){
		/*Creating our file*/
		val = mkdir(argv[1],755);
		if(val>-1){
    		printf("\n El directorio %s se creo correctamente\n",argv[1]);
		}else{
			printf("\n El directorio \"%s\" ya existe \n",argv[1]);
		}

	}else{
		printf("\nDebes incluir el nombre del directorio");
	}
	return 0;
}